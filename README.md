![Pizzería](http://www.madarme.co/portada-web.png)
# Título del proyecto:

#### Web APP - Pizzería La QQTEÑA (Lectura de datos JSON) 
***
## Índice
1. [Características](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)
***

#### Características

  - Proyecto con lectura de datos JSON a través de la API fecth JavaScript
  - Carga dinámica del JSON
  - Creación dinámica de componentes dado un valor por el usuario 
  - Archivo JSON de ejemplo: [ver](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)
***
  #### Contenido del proyecto
  - [index.html](https://gitlab.com/programacion-web---i-sem-2019/lectura-json-ii-2020-pizzeria/-/blob/master/index.html): Archivo principal de invocación a la lectura de JSON
  - [js/pizzeria.js](https://gitlab.com/programacion-web---i-sem-2019/lectura-json-ii-2020-pizzeria/-/blob/master/js/proceso.js): Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados

***
#### Tecnologías

  - HTML5
  - JavaScript
  - [TailwindCSS](https://tailwindcss.com/), Framework utilizado para el desarrollo de las vistas (Front - End)

Usted puede ver el siguiente marco conceptual sobre la API fetch:

  - [Vídeo explicativo lectura con fetch()](https://www.youtube.com/watch?v=DP7Hkr2ss_I)
  - [Guía de Mozilla - JSON](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON)
  
  ***
#### IDE

- El proyecto se desarrolla usando Visual Studio Code. [Descargar](https://code.visualstudio.com/) 
- JSON Viewer - (http://jsonviewer.stack.hu/)

***
### Instalación

Firefox Devoloper Edition - [Descargar](https://www.mozilla.org/es-ES/firefox/developer/).
El software es necesario para ver la interacción por consola y depuración del código JS


```sh
-Descargar proyecto
-Invocar página index.html desde Mozilla Firefox 
```

***
### Demo

Para ver el demo de la aplicación puede dirigirse a: [Pizzería La QQTEÑA](http://ufps33.madarme.co/pizzeria/).

***
### Autor(es)
Proyecto desarrollado por:
- Javier Eduardo Contreras Castro (<javiereduardocc@ufps.edu.co>).
- Breinner Farid Moreno Vera (<breinnerfaridmv@ufps.edu.co>)


***
### Institución Académica   
Proyecto desarrollado en la Materia Programación Web - Grupo B del [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
   
